<!DOCTYPE html>
<?php 
if($_REQUEST){
    $mal=false;
}else{
    $mal=true;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if($mal){
            
        ?>
        <form name="f" method="get">
            <div>
                <label for="ciudad">Seleccione la ciudad</label>
                <select name="ciudad">
                    <option value="0"></option>
                    <optgroup label ="Asia">
                        <option value="3">Delhi</option>
                        <option value="4">Hong Kong</option>
                        <option value="8">Mumbai</option>
                        <option value="11">Tokyo</option>
                    </optgroup>
                    <optgroup label ="Europe">
                        <option value="1">Amsterdam</option>
                        <option value="5">London</option>
                        <option value="7">Moscow</option>
                    </optgroup>
                    <optgroup label ="North America">
                        <option value="6">Los Angeles</option>
                        <option value="9">New York</option>
                    </optgroup>
                    <optgroup label ="South America">
                        <option value="2">Buenos Aires</option>
                        <option value="10">Sao Paulo</option>
                    </optgroup>
                    
                </select>
                <button>Enviar</button>
            </div>
        </form>
        <?php 
        }else{
            $ciudades=array("","Amsterdam","Buenos Aires","Delphi","Hong Kong","London","Los Angeles","Mocú","Mumbai","New York","Sao Paulo","Tokyo");
            echo "La ciudad elegida es:". $ciudades[$_GET['ciudad']];
        }
        ?>
        
    </body>
</html>
