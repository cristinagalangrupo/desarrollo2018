<?php
/**
 * Función que genera colores
 * @param int $numero El número de colores a generar
 * @return array los colores solicitados en un array de cadenas
 */
function generaColores($numero){
$colores=array();
for($n=0;$n<$numero;$n++){
    $colores[$n]="#";
    for($c=1;$c<7;$c++){
        $colores[$n].= dechex(mt_rand(0, 15));
    }
}
return$colores;
}

var_dump(generaColores(4));