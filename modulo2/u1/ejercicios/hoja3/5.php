<?php
/**
 * Función que cuenta los valores repetidos en un array
 * @param int[] $array Es el array que recibe con los números a evaluar
 * @param int[] $devolverTodos E
 * @return int[] $repetead Es el array asociativo que devuelve la función value es el número y count el nº de repeticiones ordenado por número
 */
function elementosRepetidos($array,$devolverTodos=false){
    $repeated=array();
    foreach ((array) $array as $value){
        $inArray=false;
        
        foreach ($repeated as $i=>$rItem){
            if($rItem['value']===$value){
                $inArray=true;
                ++$repeated[$i]['count'];
            }
        }
    
        if(false===$inArray){
            $i=count($repeated);
            $repeated[$i]=array();
            $repeated[$i]['value']=$value;
            $repeated[$i]['count']=1;
        }
    }
    if(!$devolverTodos){ //si la cuenta es 1 lo destruye
        foreach ($repeated as $i=>$rItem){
            if($rItem['count']===1){
                unset($repeated[$i]);
            }
        }
    }
    sort($repeated);
    return $repeated;
}

var_dump(elementosRepetidos([33,33,33,2,2,3,3]));
