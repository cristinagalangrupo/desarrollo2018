<?php
/**
 * Esta función genera una serie de números aleatorios
 * @param int $minimo Este es el valor mínimo
 * @param int $maximo Este es el valor máximo
 * @param int $numero Este es el número de valores a generar
 * @param int[] $salida Es el array donde se almacena la salida
 * @return void
 * 
 */
function ejercicio2($minimo,$maximo,$numero,&$salida){
   /* 
    * Inicializo el array para evitar que se quede con valores anteriores
    */
    $salida=array(); 
    /*
     * Bucle para rellenar el array
     */
    for($c=0;$c<$numero;$c++){
        $salida[$c]= mt_rand($minimo, $maximo);
    }
}
ejercicio2(1, 100, 10,$salida);
var_dump($salida);


