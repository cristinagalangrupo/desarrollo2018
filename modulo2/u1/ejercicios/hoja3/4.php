<?php
/**
 * Función que genera colores
 * @param int $numero El número de colores a generar
 * @param bool $almohadilla=true con el valor true nos indica que coloquemos la almohadilla 
 * @return array los colores solicitados en un array de cadenas
 */
function generaColores($numero,$almohadilla=true){
    $colores=array();
    for($n=0;$n<$numero;$n++){
        $c=0;
        $limite=6;
        $colores[$n]="";
        if($almohadilla){
            $colores[$n]="#";
            $limite=7;
        }
        for(;$c<$limite;$c++){
            $colores[$n].= dechex(mt_rand(0, 15));
        }
    }
return $colores;
}

var_dump(generaColores(6));