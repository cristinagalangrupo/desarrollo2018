<?php
/**
 * Esta función genera una serie de números aleatorios
 * @param int $minimo Este es el valor mínimo
 * @param int $maximo Este es el valor máximo
 * @param int $numero Este es el número de valores a generar
 * @return int[] el conjunto de números solicitados
 */
function ejercicio1($minimo,$maximo,$numero){
    $local=array(); // El array donde voy a colocar el resultado
    /*
     * Bucle para rellenar el array
     */
    for($c=0;$c<$numero;$c++){
        $local[$c]= mt_rand($minimo, $maximo);
    }
    /*
     * Devolver el array
     */
    return $local;
}

var_dump(ejercicio1(1,100,10));
