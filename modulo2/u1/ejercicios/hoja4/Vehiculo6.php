<?php

class Vehiculo{
    public $matricula;
    private $color;
    protected $encendido;
    public static $ruedas=5;
            
    function __construct($matricula,$color,$encendido) {
       $this->matricula=$matricula;
       $this->color=$color;
       $this->encendido=$encendido;
    }
    
    public function encender(){
        $this->encendido=false;
        echo 'Vehículo apagado<br>';
    }
    
    public function apagar(){
        $this->encendido=false;
        echo 'Vehiculo apagado <br>';
    }
    static function mensaje(){
        echo "Esto es mi coche";
    }
    
    static function ruedas(){
        echo Vehiculo::$ruedas;
    }
}