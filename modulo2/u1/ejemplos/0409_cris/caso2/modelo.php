<?php

$campos=[
    "nombre"=>[
        "label"=>"Nombre completo",
        "placeholder"=>"Escribe tu nombre",
        "value"=>"",
        "error"=>"El nombre no puede estar vacío",
        "type"=>"text"
    ],
    "edad"=>[
        "label"=>"Edad",
        "placeholder"=>"Escribe tu edad",
        "value"=>"",
        "error"=>"La edad no puede estar vacío",
        "type"=>"number"
    ],
    "telefono"=>[
        "label"=>"Teléfono",
        "placeholder"=>"Escribe tu teléfono",
        "value"=>"",
        "error"=>"El teléfono no puede estar vacío",
        "type"=>"text"
    ],
    "poblacion"=>[
        "label"=>"población",
        "placeholder"=>"Selecciona tu población",
        "value"=>"",
        "error"=>"Población no puede estar vacío",
        "type"=>"select",
        "options"=>["Torrelavega","Laredo","Potes"]
    ]
    
];

