<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo clases\cosas\caja::mostrar();
        use clases\personas\alumno\Ramon;
        echo Ramon::hola();        
        
        use clases\personas as g;
        echo g\personas::mensaje();
        
        echo clases\personas\profesor\Ramon::adios();
        
?>
    </body>
</html>
