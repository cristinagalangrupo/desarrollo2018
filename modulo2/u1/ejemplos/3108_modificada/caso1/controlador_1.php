<?php

    $formulario=1;
    
    $modelo=[
        "nombre"=>"",
        "poblacion"=>"",
        "edad"=>0,
        "telefono"=>"",
        "errores" =>[]
    ];
          
    //var_dump($_REQUEST);
    
    if(empty($_REQUEST)){
        // carga el formulario por primera vez
        echo "caso 1";
        $modelo["errores"]="Introduce todos los datos";
                
    }elseif(empty($_REQUEST["nombre"]) && empty ($_REQUEST["edad"]) && empty($_REQUEST["poblacion"]) && empty($_REQUEST["telefono"])){
        echo "caso 2";
        $modelo["errores"][]="No has escrito nada";
    }else{
        
        /* comprobar que errores tengo */
        if(empty($_REQUEST["nombre"])){
            $modelo["errores"][]="No has escrito el nombre<br>";
        }else{
            $modelo["nombre"]=$_REQUEST["nombre"];
        }
        
        if(empty($_REQUEST["poblacion"])){
            $modelo["errores"][]="No has escrito la poblacion<br>";
        }else{
            $modelo["poblacion"]=$_REQUEST["poblacion"];
        }
        
        if(empty($_REQUEST["edad"])){
            $modelo["errores"][]="No has escrito la edad<br>";
        }else{
            $modelo["edad"]=$_REQUEST["edad"];
        }
        
        if(empty($_REQUEST["telefono"])){
            $modelo["errores"][]="No has escrito el telefono<br>";
        }else{
            $modelo["telefono"]=$_REQUEST["telefono"];
        }

        if(count($modelo["errores"])==0){
            // este caso es que no hay errores
            // tienes escrito algo en todos los campos
            // quiero mostrar el resultado
            $formulario=0;
            echo "caso 4";
        }else{
            echo "caso 3";
        }        
    }
    //var_dump(empty($modelo["errores"]));
    



