<?php

    $formulario=1;
    
    $modelo=[
        "nombre"=>[
            "valor"=>"",
            "error"=>""
        ],
        "poblacion"=>[
            "valor"=>"",
            "error"=>""
        ],
        "edad"=>[
            "valor"=>"",
            "error"=>""
        ],
        "telefono"=>[
            "valor"=>"",
            "error"=>""
        ],
        "errores"=>""
    ];
     function comprobarError($arg){
         if(empty($_REQUEST[$arg])){
             return 1;
         }else{
             return 0;
         }
     }
     $textos=[
         nombre=>"No has escrito el nombre",
         poblacion=>"No has escrito la poblacion",
         edad=>"No has escrito la edad",
         telefono=>"No has escrito el telefono"
     ];
     function escribir($arg){
         
     }
    //var_dump($_REQUEST);
    
    if(empty($_REQUEST)){
        // carga el formulario por primera vez
        echo "caso 1";
        $modelo["errores"]="Introduce todos los datos";
                
    }elseif(comprobarError ("nombre")*comprobarError ("edad")*comprobarError ("poblacion")*comprobarError ("telefono")==1){
        echo "caso 2";
        $modelo["errores"]="No has escrito nada, tienes que rellenar todos los campos";
    }else{
        $modelo["errores"]=[];
        /* comprobar que errores tengo */
        if(comprobarError ("nombre")){
            $modelo["errores"][]="No has escrito el nombre";
            $modelo["nombre"]["error"]="No has escrito el nombre";
        }else{
            $modelo["nombre"]["valor"]=$_REQUEST["nombre"];
        }
        
        if(comprobarError ("poblacion")){
            $modelo["errores"][]="No has escrito la poblacion";
            $modelo["poblacion"]["error"]="No has escrito la poblacion";
        }else{
            $modelo["poblacion"]["valor"]=$_REQUEST["poblacion"];
        }
        
        if(comprobarError ("edad")){
            $modelo["errores"][]="No has escrito la edad";
            $modelo["edad"]["error"]="No has escrito la edad";
        }else{
            $modelo["edad"]["valor"]=$_REQUEST["edad"];
        }
        
        if(comprobarError ("telefono")){
            $modelo["errores"][]="No has escrito el telefono";
            $modelo["telefono"]["error"]="No has escrito el telefono";
        }else{
            $modelo["telefono"]["valor"]=$_REQUEST["telefono"];
        }

        if(count($modelo["errores"])==0){
            // este caso es que no hay errores
            // tienes escrito algo en todos los campos
            // quiero mostrar el resultado
            $formulario=0;
            echo "caso 4";
        }else{
            echo "caso 3";
        }        
    }
    //var_dump(empty($modelo["errores"]));
    



