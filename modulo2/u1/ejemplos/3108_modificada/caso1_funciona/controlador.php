<?php

    $formulario=1;
    
    $modelo=[
        "nombre"=>[
            "valor"=>"",
            "error"=>""
        ],
        "poblacion"=>[
            "valor"=>"",
            "error"=>""
        ],
        "edad"=>[
            "valor"=>"",
            "error"=>""
        ],
        "telefono"=>[
            "valor"=>"",
            "error"=>""
        ],
        "errores"=>""
    ];
          function comprobarErrores($arg,&$arg2,&$arg3){ //paso el campo, $modelo y $modelo["errores"] POR REFERENCIA
              if(empty($_REQUEST[$arg])){
                  switch ($arg){
                      case "nombre":
                          $texto="No has escrito el nombre";
                          break;
                      case "poblacion":
                          $texto="No has escrito la poblacion";
                          break;
                      case "edad":
                          $texto="No has escrito la edad";
                          break;
                      case "telefono":
                          $texto="No has escrito el teléfono";
                          break;
                  }
                  //var_dump($texto);
                  $arg3[]=$texto;
                  $arg2[$arg]["error"]=$texto;
              }else{
                  $arg2[$arg]["valor"]=$_REQUEST[$arg];
              }
          }
    //var_dump($_REQUEST);
    
    if(empty($_REQUEST)){
        // carga el formulario por primera vez
        echo "caso 1";
        $modelo["errores"]="Introduce todos los datos";
                
    }else{
        $modelo["errores"]=[];
        /* comprobar que errores tengo */
        comprobarErrores("nombre",$modelo,$modelo["errores"]);
        comprobarErrores("poblacion",$modelo,$modelo["errores"]);
        comprobarErrores("edad",$modelo,$modelo["errores"]);
        comprobarErrores("telefono",$modelo,$modelo["errores"]);
        
        //var_dump($modelo["errores"]);
        if(count($modelo["errores"])==0){
            // este caso es que no hay errores
            // tienes escrito algo en todos los campos
            // quiero mostrar el resultado
            $formulario=0;
            echo "caso 4";
        }elseif(count($modelo["errores"])==4){
            echo "caso 2";
            $modelo=[
        "nombre"=>[
            "valor"=>"",
            "error"=>""
        ],
        "poblacion"=>[
            "valor"=>"",
            "error"=>""
        ],
        "edad"=>[
            "valor"=>"",
            "error"=>""
        ],
        "telefono"=>[
            "valor"=>"",
            "error"=>""
        ],
        "errores"=>""
    ];
            $modelo["errores"]="No has escrito nada, tienes que rellenar todos los campos";
        }else{
            echo "caso 3";
        }        
    }
    //var_dump(empty($modelo["errores"]));
    



