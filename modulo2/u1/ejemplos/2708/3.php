<!DOCTYPE html>
<?php
    $bien=1; // supongo que los datos me llegan
    if(empty($_REQUEST)){
        $bien=0; //los datos no me llegan
    }
?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            div{
                margin:10px;
            }
        </style>
    </head>
    <body>
        
        <div class = "wrapper">
            <?php 
            if(!$bien){
                
            
            ?>
            <form action="3.php" method="get">
                <div>
                    <label for="nombre">Nombre</label> 
                    <input name="nombre" id="nombre" type="text">
                </div>
                <div>
                    <label for="apellidos">Apellidos</label>
                    <input name="apellidos" id="apellidos" type="text">
                </div>
                <div>
                    <label for="edad">Edad</label>
                     <input name="edad" id="edad" type="number">
                </div>
                <div>
                    <label for="poblacion">Población</label>
                     <input id="poblacion" list="poblacion" name="poblacion">
                      <datalist id="poblacion">
                          <option value="Santander">
                          <option value="Potes">
                      </datalist>
                </div>
                <div>
                     <span>Barrio</span>
                     <div>
                         <input id="n" name="barrio" value="Norte" type="radio"><label for="n">Norte</label>
                         <input id="e" name="barrio" value="Este" type="radio"><label for="e">Este</label>
                     </div>
                     <div>
                         <input id="s" name="barrio" value="Sur" type="radio"><label for="s">Sur</label>
                         <input id="o" name="barrio" value="Oeste" type="radio"><label for="o">Oeste</label>
                     </div>
                     
                </div>
                <div>
                     <label>Medio</label>
                     
                     <div>
                         <input id="b" name="medio[]" value="Bus" type="checkbox"> <label for="b">Bus</label>
                     </div>
                     <div>
                         <input id="t" name="medio[]" value="Tren" type="checkbox"> <label for="t">Tren</label>
                     </div>
                     <div>
                         <input id="m" name="medio[]" value="Moto" type="checkbox"> <label for="m">Moto</label>
                     </div>
                     <div>
                         <input id="c" name="medio[]" value="Coche" type="checkbox"> <label for="c">Coche</label>
                     </div>
                         
                </div>
                <button>Enviar</button>
            </form>
            <?php 
            }else{
                echo 'Nombre:'.$_GET['nombre'];
                echo "<br>";
                echo 'Apellidos: '.$_GET['apellidos'];
                echo "<br>";
                echo 'Edad: '. $_GET['edad'];
                echo "<br>";
                echo 'Población: '. $_GET['poblacion'];
                echo "<br>";
                echo 'Barrio: '.$_GET['barrio'];
                echo "<br>";
                echo "Medio: ";
                echo "<br>";
            if(isset($_REQUEST["medio"])){
                foreach ($_REQUEST["medio"] as $valor){
                    echo '* '.$valor;
                    echo "<br>";
                }
            }
            }
            ?>
        </div>
        
    </body>
</html>
