<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            div{
                margin:10px;
            }
        </style>
    </head>
    <body>
        
        <div class = "wrapper">
            <form action="2.php" method="get">
                <div>
                    <label for="nombre">Nombre</label> 
                    <input name="nombre" id="nombre" type="text">
                </div>
                <div>
                    <label for="apellidos">Apellidos</label>
                    <input name="apellidos" id="apellidos" type="text">
                </div>
                <div>
                    <label for="edad">Edad</label>
                     <input name="edad" id="edad" type="number">
                </div>
                <div>
                    <label for="poblacion">Población</label>
                     <input id="poblacion" list="poblacion" name="poblacion">
                     <select id="poblacion">
                          <option value="Santander">Santander</option>
                          <option value="Potes">Potes</option>
                      </select>
                </div>
                <div>
                     <span>Barrio</span>
                     <div>
                         <input id="n" name="barrio" value="Norte" type="radio"><label for="n">Norte</label>
                         <input id="e" name="barrio" value="Este" type="radio"><label for="e">Este</label>
                     </div>
                     <div>
                         <input id="s" name="barrio" value="Sur" type="radio"><label for="s">Sur</label>
                         <input id="o" name="barrio" value="Oeste" type="radio"><label for="o">Oeste</label>
                     </div>
                     
                </div>
                <div>
                     <label>Medio</label>
                     
                     <div>
                         <input id="b" name="medio[]" value="Bus" type="checkbox"> <label for="b">Bus</label>
                     </div>
                     <div>
                         <input id="t" name="medio[]" value="Tren" type="checkbox"> <label for="t">Tren</label>
                     </div>
                     <div>
                         <input id="m" name="medio[]" value="Moto" type="checkbox"> <label for="m">Moto</label>
                     </div>
                     <div>
                         <input id="c" name="medio[]" value="Coche" type="checkbox"> <label for="c">Coche</label>
                     </div>
                         
                </div>
                <button>Enviar</button>
            </form>
        </div>
        
    </body>
</html>
