<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?= $datos["titulo"]?></title>
    </head>
    <body>
        <h1><?= $datos["titulo"]; ?></h1>
        <h2><?= $datos["curso"]; ?></h2>
        <?php
            include $datos["content"];
        ?>
        <div><?= $datos["pie"]; ?></div>
    </body>
</html>

