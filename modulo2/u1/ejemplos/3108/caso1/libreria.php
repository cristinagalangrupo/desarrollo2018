<?php
/**
 * 
 * @param type $m es $campos que está en el modelo
 * $valor es el subindice (el array con todos los campos value, label, placeholder...)
 */
function leerDatos(&$m){ // $campos
    foreach($m as $indice=>&$valor){
            //$m[$indice]["value"]=$_REQUEST[$indice];
            $valor["value"]=$_REQUEST[$indice];
    }
}
function comprobar (&$m){
    $errores=[];
    foreach ($m as $indice=>$valor){
        if(empty($valor["value"])){
            $errores[$indice]=$valor["error"];
        }
    }
    return $errores;
}

function dibujarControl($datos,$errores,$nombre){
    echo "<div>";
    echo '<label for='.$nombre.'>'.$datos["label"].'</label>';
    if($datos["type"]=="select"){
        
        echo '<select name="poblacion" id="poblacion">';
        echo '<option value="">Selecciona una opción</option>';
        foreach ($datos["options"] as $v){
           echo '<option value="'.$v.'">'.$v.'</option>';
        }
        echo "</select>";
    }else{
    echo '<input type="'.$datos["type"].'" name="'.$nombre.'" id="'.$nombre.'" value="'.$datos["value"].'" placeholder="'.$datos["placeholder"].'">';
    }
      echo '</div>';
}
