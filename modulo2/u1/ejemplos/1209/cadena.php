<?php

class Cadena{
    public function __construct($valor) {
        //$this->valor=$valor;
        $this->setValor($valor);
        $this->getLongitud();
    }
    private function setValor($argumento){
        
        $this->valor=$argumento;
        $this->setLongitud();
        $this->setVocales();
    }
    
    public function getValor() {
        return $this->valor;
    }
    
    private function setLongitud(){
        $this->longitud= strlen($this->getValor());
    }
    
     public function getLongitud() {
        
        return $this->longitud;
    }
    
    private function setVocales(){
        $p=$this->getValor();
        
        $t= strtolower($p);
        
         //x=$this->valor;
         $c=0;
         $c= substr_count($t,"a")+substr_count($t,"e")+substr_count($t,"i")+substr_count($t,"o")+substr_count($t,"u");
         
         $this->vocales=$c;
    }
     public function getVocales() {
         
        return $this->vocales;
    }

    private $valor;
    private $longitud;
    private $vocales;
    
}
