<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => " <div class='alert b-1 bg-primary mb-2 p-2'>Viendo: página {page} de {pageCount}</div>",
       
                  
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'], quito la almohadilla con el nº del ítem
            'id_libros',
            'titulo',
            'autor',
            'fecha',
            'foto'=>[
                       'attribute'=>'foto',
                       'label'=>'Portada',
                       'format'=>'raw',
                        'value' => function ($datos) {
                               $url = "@web/imgs/".$datos["foto"];
                               //var_dump($url);
                               return Html::img($url, ['alt'=>'myImage','width'=>'200px'],['class'=>'img-responsive']);
                        }
                        ],
           

            //['class' => 'yii\grid\ActionColumn'], son los botones del boli del ojo y de la papelera
        ],
    ]); ?>
</div>
