DROP DATABASE IF EXISTS m3p5;
CREATE DATABASE m3p5;
USE m3p5;

CREATE TABLE noticias(
  id int NOT NULL AUTO_INCREMENT,
  titulo varchar(255),
  texto text,
  categoria varchar(255),
  PRIMARY KEY(id)
  );

INSERT INTO noticias(titulo,texto,categoria) VALUES
  ('T�tulo de la noticia 1','Texto de la noticia 1','Econom�a'),
  ('T�tulo de la noticia 2','Texto de la noticia 2','Econom�a'),
  ('T�tulo de la noticia 3','Texto de la noticia 3','Sociedad'),
  ('T�tulo de la noticia 4','Texto de la noticia 4','Tecnolog�a'),
  ('T�tulo de la noticia 5','Texto de la noticia 5','Econom�a'),
  ('T�tulo de la noticia 6','Texto de la noticia 6','Tecnolog�a'),
  ('T�tulo de la noticia 7','Texto de la noticia 7','Deportes'),
  ('T�tulo de la noticia 8','Texto de la noticia 1','Deportes');

CREATE TABLE usuarios(
  id int NOT NULL AUTO_INCREMENT,
  usuario varchar(255),
  password varchar(255),
  PRIMARY KEY(id)
  );

  INSERT INTO usuarios(usuario,password) VALUES
    ('cristina','cris');