DROP DATABASE IF EXISTS aplicacionbasica3;
CREATE DATABASE aplicacionbasica3;
USE aplicacionbasica3;

CREATE TABLE fotos(
  id int AUTO_INCREMENT,
  nombre varchar(255),
  portada bool,
  PRIMARY KEY(id)
  );
CREATE TABLE paginas(
  id int AUTO_INCREMENT,
  nombre varchar(255),
  texto varchar(255),
  PRIMARY KEY(id)
  );

INSERT INTO fotos(nombre,portada) VALUES
  ('1.jpg',1),
  ('2.jpg',0),
  ('3.jpg',1),
  ('4.jpg',0),
  ('5.jpg',1);
INSERT INTO paginas (nombre,texto) VALUES
  ('P�gina 1','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat.'),
  ('P�gina 2','Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
  ('P�gina 3','Texto de la p�gina 3');

