DROP DATABASE IF EXISTS aplicacionbasica2;
CREATE DATABASE aplicacionbasica2;
USE aplicacionbasica2;
DROP TABLE IF EXISTS noticias;
CREATE TABLE noticias
  (
    id int AUTO_INCREMENT,
    titulo varchar(50),
    texto varchar(300),
    fecha timestamp,
    foto varchar(20),
    PRIMARY KEY(id)
  );
