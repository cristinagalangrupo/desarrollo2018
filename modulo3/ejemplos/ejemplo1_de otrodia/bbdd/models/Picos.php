<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "picos".
 *
 * @property string $nombre
 * @property int $altitud
 * @property string $sistema
 *
 * @property Elevan[] $elevans
 * @property Provincias[] $provincias
 */
class Picos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'picos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['altitud'], 'integer'],
            [['nombre', 'sistema'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre del Pico',
            'altitud' => 'Altitud del Pico',
            'sistema' => 'Sistema',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElevans()
    {
        return $this->hasMany(Elevan::className(), ['pico' => 'nombre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvincias()
    {
        return $this->hasMany(Provincias::className(), ['provincia' => 'provincia'])->viaTable('elevan', ['pico' => 'nombre']);
    }
}
