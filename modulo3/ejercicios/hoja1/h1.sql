DROP DATABASE IF EXISTS h1;
CREATE DATABASE h1;
USE h1;

CREATE TABLE entradas(
  id int AUTO_INCREMENT,
  texto varchar(255),
  PRIMARY KEY(id)
  );
INSERT INTO entradas(texto) VALUES('Ejemplo de clase'),('otro ejemplo');